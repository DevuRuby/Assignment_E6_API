class Employee < ActiveRecord::Base
	 has_and_belongs_to_many :teams
	 validates :registration_id, uniqueness: true, presence: true
end
