class Api::V1::EmployeesController <  Api::ApiController
def index
		employees= Employee.all
		    render json: { data: employees}
	end

	 def show
    employee= Employee.find(params[:id])
    render json: { data: employee }
  end
def new
		@team= Team.new

	end

  	def create
  	@employee =Employee.create(emp_params)
     errors = @employee.errors
  	
     if errors.blank?
     	  @employee.reload
        render :status => :ok, :json => {:employee => @employee,:message => ('successfully_created')}
    else
    	render :status => :not_found, :json => {:message => errors.full_messages}
    end
  
  end
 
     def emp_params
     	params.require(:employee).permit(:name,:date_of_join,:designation,:registration_id )
     end
end
