class Api::V1::TeamsController <  Api::ApiController
  
		def index
		teams = Team.all
		    render json: { data: teams}
	end

	 def show
    team = Team.find(params[:id])
    render json: { data: team }
  end


	def new
		@team= Team.new

	end

  	def create
  	@team =Team.create(team_params)
    p @team
     errors = @team.errors
  	
     if errors.blank?
     	  @team.reload
        render :status => :ok, :json => {:team => @team,:message => ('team_successfully_created')}
    else
    	render :status => :not_found, :json => {:message => errors.full_messages}
    end
 
  end
 
     def team_params
     	team_params=params.require(:team).permit(:name)
     end
end


