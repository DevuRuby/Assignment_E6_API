class TeamsController < ApplicationController
before_action :authenticate_model!
	def index
		@team = Team.all
	end

  def show
    @team = Team.find(params[:id])
  end

	def new
		@team= Team.new
	end

	def create
		@team =Team.new(team_params)
	  @team.save
		redirect_to :action => 'new'
	end
 
  

  def addemployee
    if params[:team_id]
  	@team=Team.find(params[:team_id])
  	@employee=Employee.find_by_id(params[:employee][:id])
  	@team.employees << @employee
    redirect_to :action => 'index'
  end
   end

private 
     def team_params
     	params.require(:team).permit(:name)
     end
     
end




